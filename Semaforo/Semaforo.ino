/*
 * Dichiariamo la coppia di array paralleli seguendo il criterio
 * della coppia di inversi (primo indice dell'Array 1 corrisponde
 * al colore inverso del primo indice dell'Array 2).
 * @convention 
 */
int sinistraRYG[] = {13, 12, 11};
int destraGYR[] = {2, 3, 6};

int numeroPin = 3;

void setup() {
  
  for (int i = 0; i < numeroPin; i++) {
    pinMode(sinistraRYG[i], OUTPUT);
    pinMode(destraGYR[i], OUTPUT);
  }
}

void loop() {

    /* 
     * Generiamo un numero random per selezionare l'indice 
     * nella coppia di vettori da far illuminare. Scegliamo
     * un range tra 0 e 3 perchè il valore massimo viene escluso
     * così da avere un range effettivo tra [0][1][2] così da 
     * evitare di dover usare un numero determinato di iterazioni se
     * scegliessimo di usare un ciclo for. Vogliamo farlo
     * andare avanti all'infinito :)
     */
     int rndIndex = random(0, 3);
     int delayTime = 1500;
     
     /*
      * Configuriamo il codice per fare debugging sul monitor
      * seriale che ci mostra gli effettivi indici generati
      */
      Serial.begin(9600);
      Serial.println(rndIndex);

     /* Accendiamo dunque un led qualsiasi estratto
      * con indice a caso dalla funzione random() 
      */
     digitalWrite(sinistraRYG[rndIndex], HIGH);

     /*
      * Leggiamo se il led del semaforo opposto è spento,
      * in tal caso lo accendiamo così da validare la
      * coppia di contrari. (Se accendo verde dall'altra
      * parte avrò rosso e viceversa)
      */
     if (digitalRead(destraGYR[rndIndex] == LOW)) {
        digitalWrite(destraGYR[rndIndex], HIGH);
        delay(delayTime);
        digitalWrite(destraGYR[rndIndex], LOW);
     } else {
        digitalWrite(destraGYR[rndIndex], LOW);
        delay(delayTime);
        digitalWrite(destraGYR[rndIndex], HIGH);
     }
     
     digitalWrite(sinistraRYG[rndIndex], LOW);

}
